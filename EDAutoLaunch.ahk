﻿#NoEnv
#SingleInstance, Force
SendMode, Input
SetWorkingDir, %A_ScriptDir%
Update := False
For Index, Argument in A_Args {
  If (Format("{:L}", Argument) == "update") {
    Update := True
    Break
  }
}
RegRead, RegistryPath, HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\EDLaunch.exe, Debugger
If (RegistryPath != A_ScriptFullPath) {
  If (!A_IsAdmin) {
    Run, *RunAs "%A_ScriptFullPath%"
    ExitApp, 1
  }
  RegWrite, REG_SZ, HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\EDLaunch.exe, Debugger, % A_ScriptFullPath
  Update := True
}
If (Update) {
  FileCopy, EDLaunch.exe, EDManualLaunch.exe, 1
  MsgBox, 0, % "EDLaunch - Update", % "Update client and click ""Play"" to capture the launch details."
  Run, EDManualLaunch.exe
  WinWait, ahk_exe EliteDangerous64.exe
  WinGet, ProcessID, PID, ahk_exe EliteDangerous64.exe
  Success := False
  For Process In ComObjGet("WinMgmts:").ExecQuery("SELECT CommandLine FROM Win32_Process WHERE Name LIKE '%EliteDangerous%'") {
    Found := RegExMatch(Process.CommandLine, """ServerToken(.*)""", Match)
    If (Found) {
      ServerToken := Trim(Match1)
      RegWrite, REG_SZ, HKEY_CURRENT_USER\Software\Frontier Developments, ServerToken, % ServerToken
      Success := True
      Break
    }
  }
  WinClose, ahk_exe EDManualLaunch.exe
  Process, Close, % ProcessID
  If (Success) {
    MsgBox, 0, % "EDLaunch - Update", % "Update successful, you can now launch the game.", 3
  }
  Else {
    MsgBox, 0, % "EDLaunch - Update", % "Update failed!", 3
  }
}
Else {
  RegRead, ServerToken, HKEY_CURRENT_USER\Software\Frontier Developments, ServerToken
  Run, % "EliteDangerous64.exe ""ServerToken " . ServerToken . """", Products\elite-dangerous-odyssey-64
}
